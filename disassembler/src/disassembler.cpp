//
// Created by Artur Twardzik on 1/24/2021.
//

#include "disassembler.h"

void Disassembler::load_binary(const std::string &filename) {
    this->load_source_code(filename);

    std::fstream file;
    file.open(filename, std::ios::in);

    std::string line{};
    uint32_t line_number = 1;
    while (std::getline(file, line)) {
        if (line.empty() || line.starts_with(';')) blank_lines.push_back(line_number);
        ++line_number;
    }
}

//! disassemble all
void Disassembler::disassemble() {
    current_address = 0x00;
    std::cout << "---------------------------------------------------------------\n";
    std::cout << "  ADDRESS  |        MACHINE CODE        |  ASSEMBLY INSTRUCTION\n";
    std::cout << "---------------------------------------------------------------\n";
    try {
        while (this->RAM.address_already_exist(current_address)) {
            std::cout << convert_number_to_address(current_address) << " | ";

            pass_instruction_to_cpu();

            display_machine_code();

            display_assembly_instruction();

            if (error_flag) break;
        }
    }
    catch (std::exception &e) {
        return;
    }
}

//! disassemble and mark the current line
void Disassembler::disassemble(const uint32_t &instruction_address) {
    current_address = 0x00;
    std::cout << "---------------------------------------------------------------\n";
    std::cout << "  ADDRESS  |        MACHINE CODE        |  ASSEMBLY INSTRUCTION\n";
    std::cout << "---------------------------------------------------------------\n";
    try {
        uint32_t line = 1;
        while (this->RAM.address_already_exist(current_address)) {
            if (current_address == instruction_address) std::cout << "\t---> ";
            else std::cout << (line < 10 ? "0" + std::to_string(line) : std::to_string(line)) << ". | ";
            std::cout << convert_number_to_address(current_address) << " | ";

            pass_instruction_to_cpu();

            display_machine_code();

            display_assembly_instruction();

            if (error_flag) break;
            ++line;
        }
    }
    catch (std::exception &e) {
        return;
    }
}

//! disassemble from line to line
void Disassembler::disassemble(const uint32_t &line_start, const uint32_t &line_end) {
    current_address = 0x00;
    std::cout << "---------------------------------------------------------------\n";
    std::cout << "  ADDRESS  |        MACHINE CODE        |  ASSEMBLY INSTRUCTION\n";
    std::cout << "---------------------------------------------------------------\n";
    try {
        uint32_t line{};
        while (++line < line_start) {
            if (std::find(blank_lines.begin(), blank_lines.end(), line) != std::end(blank_lines)) continue;
            pass_instruction_to_cpu();
        }

        while ((this->RAM.address_already_exist(current_address)) && (line <= line_end)) {
            if (std::find(blank_lines.begin(), blank_lines.end(), line) != std::end(blank_lines)) continue;

            std::cout << (line < 10 ? "0" + std::to_string(line) : std::to_string(line)) << ". | "
                      << convert_number_to_address(current_address) << " | ";

            pass_instruction_to_cpu();

            display_machine_code();

            display_assembly_instruction();

            if (error_flag) break;
            ++line;
        }
    }
    catch (std::exception &e) {
        return;
    }
}

std::string Disassembler::convert_register_number_to_name(const uint8_t &reg) {
    std::string r{};

    if (reg < 0x10) r = "R" + std::to_string(reg);
    else if (reg == 0x10) r = "ESP";
    else if (reg == 0x11) r = "EBP";
    else if (reg == 0x12) r = "ESI";
    else if (reg == 0x13) r = "EDI";
    else if (reg == 0x14) r = "EIP";
    else r = std::to_string(reg);

    return r;
}

void Disassembler::display_assembly_instruction() {
    std::cout << std::hex << std::uppercase;

    auto print_reg = [&](const std::string &instruction) {
        std::cout << instruction << " "
                  << convert_register_number_to_name(parameters.at(0))
                  << std::endl;
    };

    auto print_number = [&](const std::string &instruction) {
        std::cout << instruction << " "
                  << convert_bytes_to_number(parameters, parameters.begin(), parameters.end())
                  << std::endl;
    };

    auto print_reg_reg = [&](const std::string &instruction) {
        std::cout << instruction << " "
                  << convert_register_number_to_name(parameters.at(0)) << ", "
                  << convert_register_number_to_name(parameters.at(1))
                  << std::endl;
    };

    auto print_reg_number = [&](const std::string &instruction) {
        auto it_begin = std::next(parameters.begin(), 1);
        std::cout << instruction << " "
                  << convert_register_number_to_name(parameters.at(0)) << ", "
                  << convert_bytes_to_number(parameters, it_begin, parameters.end()) << "h"
                  << std::endl;
    };

    auto print_reg_ptr_reg = [&](const std::string &instruction, const std::string &size) {
        std::cout << instruction << " "
                  << convert_register_number_to_name(parameters.at(0)) << ", "
                  << size << " PTR [" << convert_register_number_to_name(parameters.at(1)) << "]"
                  << std::endl;
    };

    auto print_ptr_reg_reg = [&](const std::string &instruction, const std::string &size) {
        std::cout << instruction << " "
                  << size << " PTR [" << convert_register_number_to_name(parameters.at(0)) << "], "
                  << convert_register_number_to_name(parameters.at(1))
                  << std::endl;
    };

    auto print_reg_ptr_address = [&](const std::string &instruction, const std::string &size) {
        auto it_begin = std::next(parameters.begin(), 1);
        std::cout << instruction << " "
                  << convert_register_number_to_name(parameters.at(0)) << ", "
                  << size << " PTR [0x"
                  << convert_bytes_to_number(parameters, it_begin, parameters.end()) << "]"
                  << std::endl;
    };

    auto print_ptr_reg_value = [&](const std::string &instruction, const std::string &size) {
        auto it_begin = std::next(parameters.begin(), 1);
        std::cout << instruction << " "
                  << size << " PTR [" << convert_register_number_to_name(parameters.at(0)) << "], "
                  << convert_bytes_to_number(parameters, it_begin, parameters.end())
                  << std::endl;
    };

    auto print_ptr_address_reg = [&](const std::string &instruction, const std::string &size) {
        auto it_end = std::next(parameters.begin(), 4);
        std::cout << instruction << " "
                  << size << " PTR [0x" << convert_bytes_to_number(parameters, parameters.begin(), it_end) << "], "
                  << convert_register_number_to_name(parameters.at(4))
                  << std::endl;
    };

    auto print_ptr_address_value = [&](const std::string &instruction, const std::string &size) {
        auto it_end = std::next(parameters.begin(), 4);
        std::cout << instruction << " "
                  << size << " PTR [0x" << convert_bytes_to_number(parameters, parameters.begin(), it_end) << "], "
                  << convert_bytes_to_number(parameters, it_end, parameters.end()) << "h"
                  << std::endl;
    };

    switch (this->CPU.get_instruction()) {
        case 0x00:
            std::cout << "NOP\n";
            break;
        case 0x01:
            std::cout << "POP\n";
            break;
        case 0x02:
            std::cout << "RET\n";
            break;
        case 0x05:
            print_reg("INC");
            break;
        case 0x06:
            print_reg("DEC");
            break;
        case 0x07:
            print_reg("POP");
            break;
        case 0x08:
            print_reg("PUSH");
            break;
        case 0x09:
            print_reg("NOT");
            break;
        case 0x0A:
            print_reg("RET");
            break;
        case 0x10:
            std::cout << "INT "
                      << (parameters.at(0) < 16 ? "0" : "") << parameters.at(0) << "h"
                      << std::endl;
            break;
        case 0x11:
            print_number("CALL");
            break;
        case 0x12:
            print_number("JMP");
            break;
        case 0x13:
            print_number("JE");
            break;
        case 0x14:
            print_number("JG");
            break;
        case 0x15:
            print_number("JGE");
            break;
        case 0x16:
            print_number("JL");
            break;
        case 0x17:
            print_number("JLE");
            break;
        case 0x18:
            print_number("JNE");
            break;
        case 0x19:
            print_number("PUSH");
            break;
        case 0x20:
            std::cout << "RET " << convert_bytes_to_number(parameters, parameters.begin(), parameters.end()) << "h\n";
            break;
        case 0x21:
            print_reg_reg("MOV");
            break;
        case 0x22:
            print_reg_reg("CMP");
            break;
        case 0x23:
            print_reg_reg("MUL");
            break;
        case 0x24:
            print_reg_reg("DIV");
            break;
        case 0x25:
            print_reg_reg("ADD");
            break;
        case 0x26:
            print_reg_reg("SUB");
            break;
        case 0x27:
            print_reg_reg("XOR");
            break;
        case 0x28:
            print_reg_reg("AND");
            break;
        case 0x29:
            print_reg_reg("OR");
            break;
        case 0x2A:
            print_reg_reg("TEST");
            break;
        case 0x2B:
            print_reg_ptr_reg("MOV", "BYTE");
            break;
        case 0x2C:
            print_reg_ptr_reg("MOV", "WORD");
            break;
        case 0x2D:
            print_reg_ptr_reg("MOV", "DWORD");
            break;
        case 0x2E:
            print_ptr_reg_reg("MOV", "BYTE");
            break;
        case 0x2F:
            print_ptr_reg_reg("MOV", "WORD");
            break;
        case 0x30:
            print_ptr_reg_reg("MOV", "DWORD");
            break;
        case 0x31:
            print_reg_number("MOV");
            break;
        case 0x32:
            print_reg_number("CMP");
            break;
        case 0x33:
            print_reg_number("MUL");
            break;
        case 0x34:
            print_reg_number("DIV");
            break;
        case 0x35:
            print_reg_number("ADD");
            break;
        case 0x36:
            print_reg_number("SUB");
            break;
        case 0x37:
            print_reg_number("XOR");
            break;
        case 0x38:
            print_reg_number("AND");
            break;
        case 0x39:
            print_reg_number("OR");
            break;
        case 0x3A:
            print_reg_number("TEST");
            break;
        case 0x3B:
            print_reg_ptr_address("MOV", "BYTE");
            break;
        case 0x3C:
            print_reg_ptr_address("MOV", "WORD");
            break;
        case 0x3D:
            print_reg_ptr_address("MOV", "DWORD");
            break;
        case 0x3E:
            print_ptr_reg_value("MOV", "BYTE");
            break;
        case 0x3F:
            print_ptr_reg_value("MOV", "WORD");
            break;
        case 0x40:
            print_ptr_reg_value("MOV", "DWORD");
            break;
        case 0x41:
            print_reg_number("LEA");
            break;
        case 0x43:
            print_ptr_address_reg("MOV", "BYTE");
            break;
        case 0x44:
            print_ptr_address_reg("MOV", "WORD");
            break;
        case 0x45:
            print_ptr_address_reg("MOV", "DWORD");
            break;
        case 0x52: {
            auto it_end = std::next(parameters.begin(), 4);
            std::cout << "CMP "
                      << "[" << convert_bytes_to_number(parameters, parameters.begin(), it_end) << "h], "
                      << convert_bytes_to_number(parameters, it_end, parameters.end()) << "h"
                      << std::endl;
        }
            break;
        case 0x53:
            print_ptr_address_value("MOV", "BYTE");
            break;
        case 0x54:
            print_ptr_address_value("MOV", "WORD");
            break;
        case 0x55:
            print_ptr_address_value("MOV", "DWORD");
            break;
        case 0x61:
            print_reg_reg("SHL");
            break;
        case 0x62:
            print_reg_reg("SHR");
            break;
        case 0x71:
            print_reg_number("SHL");
            break;
        case 0x72:
            print_reg_number("SHR");
            break;
    }
    std::cout << std::dec << std::nouppercase;
}

void Disassembler::display_machine_code() {
    std::stringstream str;
    for (auto n : parameters) {
        if (n < 16) str << "0";
        str << std::hex << std::uppercase << n << " ";
    }

    std::string machine_code = str.str();
    while (machine_code.length() < 24) machine_code += " ";

    std::cout << std::hex << std::uppercase <<
              (opcode < 16 ? "0" : "") << opcode << " "
              << std::dec << std::nouppercase;
    std::cout << machine_code << "|   ";
}

std::string Disassembler::convert_number_to_address(const uint32_t &number) {
    std::stringstream str;
    str << std::hex << std::uppercase;
    str << "0x" << number;

    std::string num = str.str();

    while (num.length() < 10) {
        num.insert(2, "0");
    }
    return num;
}

std::string Disassembler::convert_bytes_to_number(std::vector<uint32_t> &bytes_to_convert,
                                                  std::vector<uint32_t>::iterator first,
                                                  std::vector<uint32_t>::iterator last) {

    auto dec2hex = [](const uint32_t &number) {
        std::stringstream str;
        str << std::hex << std::uppercase;
        if (number < 16) str << 0;
        str << number;

        return str.str();
    };

    std::string bytes{};
    for (auto byte = first; byte != last; ++byte) {
        bytes += dec2hex(*byte);
    }

    return bytes;
}


void Disassembler::pass_instruction_to_cpu() {
    opcode = RAM.get_value(current_address, 1);
    ++current_address;

    CPU.set_instruction(opcode);

    parameters.clear();
    auto next_instruction_address = current_address + CPU.get_parameters_size();
    while (current_address < next_instruction_address) {
        parameters.push_back(RAM.get_value(current_address++, 1));
    }

    CPU.set_instruction_parameters(parameters);
}
