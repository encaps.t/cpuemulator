//
// Created by Artur Twardzik on 1/24/2021.
//

#ifndef TESTS_DISASSEMBLER_H
#define TESTS_DISASSEMBLER_H

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

#include "../../core/src/bus.h"

class Disassembler : public bus::bus {
private:
    uint32_t current_address{0x00};
    uint32_t opcode;
    std::vector<uint32_t> parameters;

    std::vector<uint32_t> blank_lines;
    bool error_flag{false};
public:
    Disassembler() = default;

    void load_binary(const std::string &filename);

    void disassemble();

    void disassemble(const uint32_t &instruction_address);

    void disassemble(const uint32_t &line_start, const uint32_t &line_end);

    void display_assembly_instruction();

    void display_machine_code();

    void pass_instruction_to_cpu();

    std::string convert_register_number_to_name(const uint8_t &reg);

    std::string convert_bytes_to_number(std::vector<uint32_t> &bytes_to_convert,
                                        std::vector<uint32_t>::iterator first,
                                        std::vector<uint32_t>::iterator last);

    std::string convert_number_to_address(const uint32_t &number);
};

#endif //TESTS_DISASSEMBLER_H
