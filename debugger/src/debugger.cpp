//
// Created by Artur Twardzik on 2/27/2021.
//

#include "debugger.h"

#include <utility>

void Debugger::add_instruction(const uint32_t &line_number, const std::string &instruction) {
    std::vector<std::string> lines;

    std::fstream file;
    file.open(filename, std::ios::in);

    uint32_t current_line_number{};
    std::string current_line{};
    while (std::getline(file, current_line)) {
        ++current_line_number;
        if (current_line_number == line_number) lines.push_back(instruction);

        lines.push_back(current_line);
        original_file.push_back(current_line);
    }

    file.close();
    file.open(filename, std::ios::out);

    for (const auto &new_line : lines) file << new_line << "\n";

    file.close();
}

void Debugger::restore_file() {
    std::fstream file;
    file.open(filename, std::ios::out);

    for (const auto &original_line : original_file) file << original_line << "\n";

    file.close();
}

Debugger::Debugger(std::string file_name, const uint32_t &breakpoint_line, const uint32_t &ram_size)
        : filename(std::move(file_name)) {

    if (!filename.ends_with(".ce")) {
        if (breakpoint_line) add_instruction(breakpoint_line, "INT 03H");

        Assembler as(filename, "", true);
        as.run();

        compiled_file = filename.substr(0, filename.find('.')) + ".ce";
    }
    else {
        if (breakpoint_line) add_instruction(breakpoint_line, "1003");
        compiled_file = filename;
    }

    this->address_memory(ram_size);
    this->load_source_code(compiled_file);

    if (breakpoint_line) restore_file();

    di = std::make_unique<Disassembler>();
    di->address_memory(ram_size);
    di->load_binary(compiled_file);

    while (bus::bus::next_instruction && !bus::bus::tf_set) {
        this->transfer_instruction();
        this->execute_instruction();
        ++current_executed_line;
    }
    if (bus::bus::next_instruction == false) break_debugging = true;
}

void Debugger::execute_operation(const uint32_t &operation) {
    switch (operation) {
        case 0:
            this->transfer_instruction();
            this->execute_instruction();

            ++current_executed_line;
            break;
        case 1:
            di->disassemble(this->CPU.get_eip());
            break;
        case 2:
            this->print_registers();
            break;
        case 3:
            print_ram();
            break;
        case 4: {
            std::cout << "Function name: ";
            std::string func_name{};
            std::getline(std::cin, func_name);

            auto function_location = get_function_location(func_name);
            if (!function_location.first && !function_location.second) {
                std::cout << "Function with name: <" << func_name << "> not found in assembly.\n";
                break;
            }

            di->disassemble(function_location.first, function_location.second);
        }
            break;
        case 5: {
            //The line in this case is not the line in assembly but in machine code!
            std::cout << "Instruction number: ";
            uint32_t line_number{};
            std::cin >> line_number;
            if (std::cin.fail()) {
                std::cin.clear();
                std::cin.ignore(INT_MAX, '\n');
                std::cout << "Wrong number, try again.\n";
                break;
            }

            if (line_number <= current_executed_line) {
                std::cout << "Line number must be greater than current executed line.\n";
                break;
            }

            for (size_t i = 0; i < line_number - current_executed_line; ++i) {
                this->transfer_instruction();
                this->execute_instruction();
            }
            current_executed_line = line_number;
        }
            break;
        case 6:
            break_debugging = true;
            break;
    }

    if (bus::bus::next_instruction == false) break_debugging = true;
}

void Debugger::print_ram() {
    auto memory = this->get_memory();
    std::vector<uint8_t> machine_code;

    size_t i = 0;
    std::cout << std::hex;
    for (const auto &element : memory) {
        if ((i % 8 == 0) || (i == 0)) {
            auto addr = debug::dec2hex(element.first, true);
            while (addr.length() < 10) addr.insert(2, "0");

            std::cout << std::endl << addr << " | ";
        }

        machine_code.push_back(element.second);
        std::cout << (element.second < 0x10 ? "0" : "") << static_cast<uint32_t>(element.second) << " ";

        ++i;
        if (i % 8 == 0) {
            print_characters_representation(machine_code);
            machine_code.clear();
        }
    }
    if (i % 8 != 0) {
        auto zero_amount = 8 - machine_code.size();
        for (size_t j = 0; j < zero_amount; ++j) {
            machine_code.push_back(0x00);
            std::cout << "   ";
        }

        print_characters_representation(machine_code);
    }

    std::cout << std::dec;
}

void Debugger::print_characters_representation(const std::vector<uint8_t> &text_under_machine_code) const {
    std::cout << "\t| ";
    for (auto c : text_under_machine_code) {
        if ((c >= 32) && (c <= 126)) std::cout << c;
        else std::cout << ".";
    }
    std::cout << " |";
}

std::pair<uint32_t, uint32_t> Debugger::get_function_location(std::string function_name) {
    std::transform(function_name.begin(), function_name.end(),
                   function_name.begin(), std::toupper);
    std::string demanded_function = ";--fn--:" + function_name;

    std::fstream file;
    file.open(compiled_file, std::ios::in);

    std::string current_line{};
    uint32_t line_counter{};
    uint32_t line_begin{};
    uint32_t line_end{};
    while (std::getline(file, current_line)) {
        ++line_counter;

        if (current_line == demanded_function) {
            line_begin = line_counter + 1;
        }
        else if (line_begin && current_line.starts_with(";--fn--:")) {
            line_end = line_counter - 1;
            break;
        }
    }

    file.close();

    return std::make_pair(line_begin, line_end);
}
