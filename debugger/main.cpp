//
// Created by Artur Twardzik on 2/27/2021.
//

#include "src/debugger.h"
#include "../lib/argument_parser.h"

/* PARAMETERS:
 * -s RAM size, default 65535 bytes, max 4 GB
 * -b breakpoint line
 * */

int main(int argc, char **argv) {
    std::string source_file{};
    uint32_t memory_size = 0xffff;
    uint32_t breakpoint_line{};
#ifndef DEBUG
    if (argc < 2) {
        std::cerr << "fatal error: No input files\n";
        return -1;
    }

    Parser parser(argc, argv);

    source_file = parser.get_source_file();
    if (parser.is_parameter_set("-s")) {
        memory_size = std::stoi(parser.get_value("-s"));
    }
    if (parser.is_parameter_set("-b")) {
        breakpoint_line = std::stoi(parser.get_value("-b"));
    }

#else
    source_file = "source.a";
#endif

    try {
        Debugger debugger(source_file, breakpoint_line, memory_size);

        while (!Debugger::break_debugging) {
            std::cout << "\n\n"
                      << "-------------------- \n"
                      << "[0] Next Instruction \n"
                      << "[1] Disassemble \n"
                      << "[2] Print Registers \n"
                      << "[3] Print RAM \n"
                      << "[4] Disassemble Function \n"
                      << "[5] Jump to instruction (see disassembly to know instruction's number)\n"
                      << "[6] Break Debugging \n\n"
                      << ">>> ";

            uint32_t option{};
            std::cin >> option;
            std::cout << std::endl;

            if (std::cin.fail()) {
                std::cin.clear();
                std::cin.ignore(INT32_MAX, '\n');

                std::cout << "[!] Wrong option, try again.\n";
                continue;
            }
            else if (option > 6) {
                std::cout << "[!] Wrong option, try again.\n";
                continue;
            }

            std::getchar(); //delete rubbish from the input
            debugger.execute_operation(option);
        }
    }
    catch (std::exception &e) {
        std::cout << "Process returned: " << e.what() << "\nAborting . . . ";
    }
}
