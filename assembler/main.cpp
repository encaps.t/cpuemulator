//
// Created by Artur Twardzik on 1/25/2021.
//

#include "src/assembler.h"
#include "../lib/argument_parser.h"

/* PARAMETERS:
 * -o name of output file
 * --d debug mode; add additional comments to machine code
 * */

int main(int argc, char **argv) {
    std::string source_file{};
    std::string output_file{};
    bool debug_mode = false;
#ifndef DEBUG
    if (argc < 2) {
        std::cerr << "fatal error: No input files\n";
        return -1;
    }

    Parser parser(argc, argv);

    source_file = parser.get_source_file();
    if (parser.is_parameter_set("-o")) {
        output_file = parser.get_value("-o");
    }
    if (parser.is_parameter_set("--d")) {
        debug_mode = true;
    }

#else
    source_file = "source.a";
    debug_mode = true;
#endif
    try {
        Assembler as(source_file, output_file, debug_mode);
        as.run();
    }
    catch (std::exception &e) {
        std::cout << e.what();
    }
    return 0;
}
