55 00 00 AB CD 00 00 00 00  ; RESERVE 4 BYTES
31 00 00 00 00 03           ; MOV R0, 03 - SYS_READ
31 01 00 00 00 00           ; MOV R1, 00 - STDIN
31 02 00 00 AB CD           ; MOV R2, address begin
31 03 00 00 00 04           ; MOV R3, number of bytes to read
10 80                       ; INT 80h

31 00 00 00 00 04           ; MOV R0, 04 - SYS_WRITE
31 01 00 00 00 01           ; MOV R1, 01 - STDOUT
31 02 00 00 AB CD           ; MOV R2, [0000ABCD] - string begin
31 03 00 00 00 04           ; MOV R3, 8 - number of bytes to write
10 80                       ; INT 80h

;;;;;;;;;; EXIT ;;;;;;;;;;;;;;;
31 00 00 00 00 01   ; MOV R0, 1
27 01 01            ; XOR R1, R1
10 80               ; INT 80h