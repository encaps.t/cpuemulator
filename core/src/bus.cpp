//
// Created by Artur Twardzik on 1/1/2021.
//

#include "bus.h"

void bus::bus::load_source_code(const std::string &filename) {
    std::fstream file{};
    file.open(filename, std::ios::in);
    if (!file.is_open()) throw std::runtime_error("[!] Could not open the file\n");

    std::string line{};
    std::vector<uint8_t> instructions{};
    while (std::getline(file, line)) {
        int comment_position = line.find(';');
        if (comment_position != std::string::npos) line = line.substr(0, comment_position);

        line.erase(std::remove_if(line.begin(), line.end(), [](unsigned char x) {
            return std::isspace(x);
        }), line.end());

        for (size_t byte = 0; byte < line.length(); byte += 2) {
            std::string temp = line.substr(byte, 2);
            instructions.push_back(std::stoi(temp, nullptr, 16));
        }
    }
    RAM.load_program(instructions);
}

void bus::bus::address_memory(const uint32_t &bytes_to_allocate) {
    if (bytes_to_allocate > 0xffffffff) {
        throw std::length_error("Allocation not possible. Cannot address more than 4[GB] of memory");
    }
    CPU.set_esp(bytes_to_allocate);
    RAM.memory_size = bytes_to_allocate;
    RAM.stack_head = bytes_to_allocate;
}

void bus::bus::transfer_instruction() {
    uint32_t instruction_pointer = CPU.get_eip();
    uint8_t opcode = RAM.get_value(instruction_pointer, 1);

    CPU.set_instruction(opcode);

    std::vector<uint32_t> params;
    for (size_t parameter = 0; parameter < CPU.get_parameters_size(); ++parameter) {
        params.push_back(RAM.get_value(++instruction_pointer, 1));
    }
    CPU.set_instruction_parameters(params);
}

void bus::bus::execute_instruction() {
    CPU.execute(RAM);
    //EIP is incremented always after execution - on the end of program it points to
    //value 1 greater than text section
    if (cpu::control_unit::sys_exit) next_instruction = false;
    if (CPU.tf_set()) tf_set = true;
}
